#!/usr/bin/env python
# encoding: utf-8
"""
Tests for CS Sentiment package.

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""

from unittest import TestCase

from torch import Tensor, tensor

from cs_sentiment import sentimentclassifier, helpers


class TestModel(TestCase):

    def test_get_predictions(self):
        model = sentimentclassifier.SentimentClassifier("sentiment-all-best")
        predictions = model.get_predictions(
            ["První věta se skvělým sentimentem",
             "Druhá věta s příšerně špatným sentimentem"])
        self.assertEqual(type(predictions), Tensor)
        predictions = predictions.tolist()
        self.assertEqual(len(predictions), 2)  # predictions for two sentences
        self.assertEqual(len(predictions[0]), 3)  # three labels prediction
        for doc in predictions:
            for prediction in doc:
                self.assertTrue(0 <= prediction <= 1)  # value between 0-1 included

    def test_single_sentiment_value(self):
        test_data = [
            {"prediction": tensor([1.0, 0.0, 0.0]), "value": -1.0},
            {"prediction": tensor([0.0, 0.0, 1.0]), "value": 1.0},
            {"prediction": tensor([0.0, 0.5, 1.0]), "value": 0.75},
            {"prediction": tensor([1.0, 1.0, 0.5]), "value": -0.25}
        ]
        for td in test_data:
            value = sentimentclassifier.single_sentiment_value(td["prediction"])
            self.assertEqual(type(value), float)
            self.assertTrue(-1 <= value <= 1)  # value between 0-1 included
            self.assertEqual(value, td["value"])

    def test_textual_label(self):
        test_data = [
            {"prediction": tensor([0.99, 0.0, 0.0]), "value": "negative"},
            {"prediction": tensor([0.0, 0.0, 0.99]), "value": "positive"},
            {"prediction": tensor([0.0, 0.5, 0.99]), "value": "positive"},
            {"prediction": tensor([0.90, 0.99, 0.5]), "value": "neutral"}
        ]
        for td in test_data:
            value = sentimentclassifier.textual_label(td["prediction"])
            self.assertEqual(type(value), str)
            self.assertEqual(value, td["value"])

    def test_callable(self):
        docs = [
            {"contentid": 12345, "fulltext": "První věta se skvělým sentimentem"},
            {"contentid": 67890, "fulltext": "Druhá věta s příšerně špatným sentimentem"},
        ]
        model = sentimentclassifier.SentimentClassifier("sentiment-all-best")
        predictions = model(docs, sentimentclassifier.single_sentiment_value)
        self.assertEqual(len(predictions), 2)  # two documents
        prediction = predictions[0]
        self.assertEqual(type(prediction), dict)  # return dictionary
        self.assertTrue("contentid", "sentiment" in prediction)  # dictionary has mandatory types
        self.assertEqual(type(prediction["contentid"]), int)
        self.assertEqual(type(prediction["sentiment"]), float)


class TestApi(TestCase):
    def test_compare_documents(self):
        a = [{"contentid": 12345, "features": {"cimple/sentiment": 0.99}},
             {"contentid": 67890, "features": {"cimple/sentiment": 0.0}}]
        a1 = [{"contentid": 12345, "sentiment": 0.99},
              {"contentid": 67890, "sentiment": 0.0}]
        b = [{"contentid": 12345, "sentiment": 0.99},
             {"contentid": 67890, "sentiment": 0.5}]
        self.assertTrue(helpers.compare_documents_by_attributes(a, a1, "features", "sentiment"))
        self.assertFalse(helpers.compare_documents_by_attributes(a, b, "features", "sentiment"))
