# CS Sentiment Module

CS Sentiment Module is a tool that enriches documents in the [WebLyzard](https://www.weblyzard.com/) platform
with sentiment. Sentiment is predicted by sequence classifier model based on 
Electra architecture. The tool is one of the outputs of [CIMPLE project](https://www.chistera.eu/projects/cimple) cofunded by [Technology Agency of the Czech Republic](https://www.tacr.cz/en/) as the project number [TH74010002](https://starfos.tacr.cz/en/project/TH74010002).

The tool can store three types of sentiment value in the WebLyzard platform:
1. Single numeric value from `-1` to `1`, where `1` is positive sentiment and `-1` negative sentiment.
2. One of three textual labels: `negative`, `neutral`, `positive`.
3. List of three scores that shows how strongly a label [`negative`, `neutral`, `positive`] explains a document sentiment.

The selected output can be configured as `sentiment_value` in [configuration file](config.json) as
`single`, `label`, or `labels_scores`. By default, single numeric value is returned. 

## Configuration

Default configuration is stored in [`config.json`](config.json) file.

## Dependencies

Detailed list of used dependencies is located in [`requirements-conda.txt`](requirements-conda.txt) 
and [`requirements-pip.txt`](requirements-pip.txt) files.

It is recommended tu update conda package before dependencies installation.

```
conda update -n base -c defaults conda
```

The files could be used for dependencies installation into a new conda environment using:

```
conda create --name <env> --file requirements-conda.txt -c pytorch -c conda-forge
conda activate <env> && pip install -r requirements-pip.txt
```

where `<env>` represents name of the new conda environment.

## Classification model

Classification model should be based on [Small-E-Czech](https://github.com/seznam/small-e-czech) 
language model that is used as tokenizer in this tool. 
Model should be prepared as a folder with PyTorch binary format and transformers configuration. 
The model folder should be stored in a directory inside the `models` directory. 
The model will be loaded by a name stated in the [configuration file](config.json) under `model` key. 
The name should correspond with the model folder name.

## Execution

After installation of dependencies and storing the model in the `model` folder, 
the program will execute as 

```
conda activate <env>
python -m cs_sentiment -c <configfile>
```
where `<configfile>` is a configuration file path
and `<env>` represents name of the new conda environment.

## Results

Beside the updated documents in the WebLyzard platform, 
a structured file with tabulator separated 
timestamp, document ID, and sentiment value is stored in the log file, 
according to `logfile` key in [configuration file](config.json). 

