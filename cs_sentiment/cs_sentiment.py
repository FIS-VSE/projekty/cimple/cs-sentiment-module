"""
Main module of the CS Sentiment Module package with `cs_sentiment` function.

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""
import time
import logging

from .wlapi import WlApi
from .sentimentclassifier import SentimentClassifier, sentiment_function


def cs_sentiment(config: dict) -> None:
    """
    Get documents from WL API, analyse sentiment and put sentiment to WL API.

    :param config: configuration dictionary.
    """
    # get documents from WL API
    wlapi = WlApi(config)
    docs = wlapi.get_documents()

    # initialize model and analyse sentiment
    model = SentimentClassifier(config["model"])
    docs_sentiment = model(docs, sentiment_function(config["sentiment_value"]))

    # put sentiment back to WL API
    wlapi.put_sentiment(docs_sentiment)

    # verify sentiment value on WL API
    logging.debug("Sleeping " + str(config["sleep_after_update"]) + " seconds before data verification.")
    time.sleep(config["sleep_after_update"])
    if wlapi.verify(docs_sentiment):
        logging.info("Task was successfully verified.")
