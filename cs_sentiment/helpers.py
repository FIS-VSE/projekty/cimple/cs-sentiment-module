"""
Module contains helper functions that are not handling specifically WL API or sentiment analysis issues.

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""
import datetime
import logging
from typing import Any

import pandas as pd


def lists_from_docs(docs: list[dict],
                    contentid="contentid", fulltext="fulltext") -> tuple[list[int], list[str]]:
    """
    Return contentids and fulltexts from list of documents.

    In the list of documents, a document is a dictionary, that must
    contain number identifier of a document and document fulltext.
    The contentid and fulltext keys are passed in optional parameters.

    :param docs: List of documents.
    :param contentid: Key of contentid parameter, "contentid" by default.
    :param fulltext: Key of fulltext parameter, "fulltext" by default.
    :return: Tuple of contentid list and fulltext list.
    """
    contentids = []
    fulltexts = []
    for doc in docs:
        contentids.append(doc[contentid])
        fulltexts.append(doc[fulltext])
    return contentids, fulltexts


def docs_from_lists(contentids: list[int], sentiment_values: list[Any],
                    contentid_key="contentid", sentiment_key="sentiment") -> list[dict]:
    """
    Convert list of contentids and sentiment fields to list of documents.

    :param contentids: List of document ids.
    :param sentiment_values: List of sentiment fields.
    :param contentid_key: Key for document ID for document dictionary.
    :param sentiment_key: Key for sentiment value for document dictionary.
    :return: List of documents with document id and sentiment keys.
    """
    docs = []
    for c, s in zip(contentids, sentiment_values):
        docs.append({contentid_key: c, sentiment_key: s})
    return docs


def strip_feature(feature: Any) -> Any:
    """
    Strip feature in a dictionary to float, string or list.

    Function recursively tries to read the first key of dictionary unless
    the value is of float, string, or list type.

    :param feature: feature to be stripped to value of predefined type.
    :return: value of predefined type.
    """
    if type(feature) in [float, list, str]:
        return feature
    if type(feature) is dict:
        values = list(feature.values())
        if len(values) > 0:
            return strip_feature(values[0])
        return feature
    logging.warning("Validation failed due to incompatible types")
    return feature


def write_processed_sentiment(file_path: str, docs_sentiment: list[dict]) -> None:
    """
    Write results of sentiment analysis to a file.

    Results will tab separated in a format of date and time, sentiment value, and document ID.

    :param file_path: path to a file to where result of sentiment analysis should be written.
    :param docs_sentiment: list of documents after sentiment analysis with `sentiment` field.
    """
    try:
        with open(file_path, 'a') as f:
            for doc in docs_sentiment:
                f.write("{2}\t{1}\t{0}\n".format(doc["contentid"], doc["sentiment"], datetime.datetime.now()))
    except IOError as e:
        logging.error("I/O error({0}): {1}".format(e.errno, e.strerror))


def compare_documents_by_attributes(a: list[dict], b: list[dict], key_a: str, key_b: str) -> bool:
    """
    Compare sentiment field of two lists of documents.

    :param a: first list of documents.
    :param b: second list of documents.
    :param key_a: key of the first list to be compared on.
    :param key_b: key of the second list to be compared on.
    :return: True if values of the given keys are equals on both sets of documents.
    """
    df_a = pd.DataFrame.from_records(a, index="contentid").rename(columns={key_a: "a"})
    df_b = pd.DataFrame.from_records(b, index="contentid").rename(columns={key_b: "b"})
    df = df_a.join(df_b)
    df = df.apply(lambda col: [strip_feature(x) for x in col], axis=0)

    # compare value on document pairs
    df = df.apply(lambda doc: (doc["a"] == doc["b"]), axis=1)
    return df.all(axis=None)
