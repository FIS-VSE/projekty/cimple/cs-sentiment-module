"""
This module handles API requests to Web Lyzard platform.

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""
import json
import logging
import sys

import ndjson
import requests
import requests.auth

from .helpers import write_processed_sentiment, compare_documents_by_attributes


class BearerAuth(requests.auth.AuthBase):
    """
    Callable class to be passed in auth parameter of HTTPS requests.
    """

    def __init__(self, token):
        """
        Store token for header creation.
        """
        self.token = token

    def __call__(self, request):
        """
        Add the authorization header to request object.

        :return: request object with the authorization header containing a bearer token.
        """
        request.headers["authorization"] = "Bearer " + self.token
        return request


def create_query_for_ids(docs_to_return: list[dict]) -> dict:
    """
    Prepare query field for document search base on document IDs (contentid).

    :param docs_to_return: dictionary with contentid values of documents to be returned.
    :return: query value for Query DSL that say to return only specific contentid values.
    """
    conditions = []
    for doc_to_return in docs_to_return:
        conditions.append({"contentid": {"eq": doc_to_return["contentid"]}})
    return {"bool": {"should": conditions}}


def get_docs_from_response(search_response: requests.Response) -> list[dict]:
    """
    Parse :class:`requests.Response <requests.Response>` object and get returned documents.

    :param search_response: response returned from WL Search API POST request
    :return: list of document objects from search request response.
    """
    if not search_response.ok:
        logging.error("Error getting documents from WL Search API. Code: " + str(
            search_response.status_code) + "text: " + search_response.text)
        sys.exit(1)
    search_response_data = json.loads(search_response.text)
    search_response_result = search_response_data["result"]
    if "hits" not in search_response_result:
        logging.error("No docs in search result")
        sys.exit(1)
    docs = search_response_result["hits"]
    logging.info("Got " + str(len(docs)) + " documents from Search API")
    logging.debug("Documents: " + str(docs))
    return docs


def create_put_data(contentid: str, sentiment: float) -> dict:
    """
    Prepare dictionary with sentiment for one document in WL API PUT request format.

    :param contentid: contentid of a document which sentiment is being updated.
    :param sentiment: sentiment value.
    :return: json data to be passed to PUT request as a whole or a line in ndjson format.
    """
    return {
        "contentid": contentid,
        "features": {
            "cimple/sentiment": sentiment
        }
    }


class WlApi:
    """
    WlApi allows getting documents and put sentiment information back to WL API.
    """

    def __init__(self, config: dict):
        """
        Save config and make an authorization request on WL API.

        :param config: configuration dictionary.
        """
        self.__config = config
        self.__authorize()

    def __authorize(self):
        """Make authorization request and prepare an authorization object."""
        response_token = requests.get(self.__config["base_url"] + "token",
                                      auth=(self.__config["username"],
                                            self.__config["password"]))
        if response_token.ok:
            self.__auth = BearerAuth(response_token.text)
            logging.debug("Successfully authorized on WL API with token " + response_token.text)
        else:
            logging.error("Authorization failed: " + response_token.text)

    def __get_search_config(self, config_name) -> dict:
        """
        Prepare a search config based on a search configuration name.

        There are two configuration names from `search_config_stubs` configuration field in use:
            - `untouched_articles`, for documents without sentiment value
            - `contentids`, for search of specific documents by their ID
        The specified configurations are combined with common configuration
        stated in `search_common` configuration field.

        :param config_name: name of search configuration.
        :return: search configuration dictionary that could be used as data in HTTP POST to WL API.
        """
        search_config = self.__config["search_config_stubs"][config_name].copy()
        for key, value in self.__config["search_common"].items():
            search_config[key] = value
        return search_config

    def get_documents(self) -> list[dict]:
        """
        Prepare an HTTP request on WL API and get documents from the response.

        :return: list of documents from WL API.
        """
        config = self.__get_search_config("untouched_articles")
        url = self.__config["base_url"] + self.__config["endpoint_search"]
        response = requests.post(url, json=config, auth=self.__auth)
        return get_docs_from_response(response)

    def put_sentiment(self, docs_sentiment: list[dict]) -> None:
        """
        Create PUT request to WL API for list of documents with sentiment field.

        :param docs_sentiment: list of documents with sentiment field.
        """
        url = self.__config["base_url"] + self.__config["endpoint_update"]
        put_data = []
        for doc in docs_sentiment:
            put_data.append(create_put_data(doc["contentid"], doc["sentiment"]))
        logging.debug("Sentiment data in PUT data format: " + str(put_data))
        data = ndjson.dumps(put_data)
        response = requests.put(url,
                                headers={"Content-Type": "application/x-ndjson"},
                                auth=self.__auth,
                                data=data)
        if not response.ok:
            logging.error("Error while document update. Code: " + str(response.status_code) + "text: " + response.text)
            sys.exit(1)
        logging.info("Documents update result: " + str(json.loads(response.content)))
        write_processed_sentiment(self.__config["logfile"], docs_sentiment)

    def verify(self, docs_sentiment: list[dict]) -> bool:
        """
        Verify that result of sentiment analysis are stored in WL platform.

        :param docs_sentiment: list of documents with sentiment field after sentiment analysis.
        :return: True if sentiment field on WL platform equals results of sentiment analysis.
        """
        logging.debug("Running verification of updated documents.")
        config = self.__get_search_config("contentids")
        config["query"] = create_query_for_ids(docs_sentiment)
        url = self.__config["base_url"] + self.__config["endpoint_search"]
        response = requests.post(url, json=config, auth=self.__auth)
        docs = get_docs_from_response(response)
        eq = compare_documents_by_attributes(docs_sentiment, docs, "sentiment", "features")
        if not eq:
            logging.warning("Sentiment information of analysed documents and returned documents differs, "
                            "or returned documents are missing sentiment field")
        return eq
