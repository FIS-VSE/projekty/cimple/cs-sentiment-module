"""
CS Sentiment Module
===================

Sentiment analysis for Czech language with connector to WebLyzard platform API.

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""

__version__ = "1.0.0"

__copyright__ = "Copyright (c) 2022 Prague University of Economics and Business"
__license__ = "MIT, see LICENCE for more details"
__author__ = "Filip Vencovsky"

__docformat__ = 'reStructuredText'


from os.path import dirname, abspath

ROOT_DIR = dirname(abspath(__file__))
