"""
This module handles sentiment analysis.
"""
import logging
from collections.abc import Callable
from typing import Any

import torch
import torch.nn.functional
import transformers as trf
from torch import Tensor

from .helpers import docs_from_lists, lists_from_docs


def labels_scores(predictions: Tensor) -> list[float]:
    """
    Convert predictions to list of float scores.

    :param predictions: predictions as a result of sentiment analysis of one document.
    :return: list of scores in order: negative, neutral, and positive.
    """
    list_of_predictions = predictions.tolist()
    return list_of_predictions


def single_sentiment_value(predictions: Tensor) -> float:
    """
    Combine predictions to one sentiment value.

    The sentiment value is calculated as
    (-1 * negative + 1 * positive) * (1 - neutral / 2).

    :param predictions: predictions as a result of sentiment analysis of one document.
    :return: float value of combined sentiment label scores.
    """
    neg = predictions[0]
    neu = predictions[1]
    pos = predictions[2]
    value = (-1 * neg + 1 * pos) * (1 - neu / 2)
    value = value.tolist()  # from Tensor to float
    value = round(value, 4)  # round on 4 decimal spaces
    return value


def textual_label(predictions: Tensor) -> str:
    """
    Return label name of the highest score in a prediction.

    :param predictions: predictions as a result of sentiment analysis of one document.
    :return: label name with the highest score.
    """
    label_id = torch.argmax(predictions).tolist()
    id2label = ["negative", "neutral", "positive"]
    return id2label[label_id]


def sentiment_function(name: str) -> Callable[[Tensor], Any]:
    """
    Return function that converts sentiment prediction to a value based on a converter name.

    Available names:
        - `single`, for combined sentiment value
        - `label`, for textual label
        - `label_scores`, for list of scores for each label.

    :param name: name of a converter.
    :return: callable function that convert prediction to sentiment value.
    """
    sentiment_functions = {
        "single": single_sentiment_value,
        "label": textual_label,
        "labels_scores": labels_scores
    }
    return sentiment_functions[name]


class SentimentClassifier:
    """
    SentimentClassifier is a callable class for sentiment analysis.

    Classifier is based on a pretrained Electra model Small-E-Czech and
    a custom classifier on the same architecture that should be available
    in the project directory.
    """
    def __init__(self, model_name: str):
        """
        Load tokenizer and custom classifier model.

        :param model_name: name of custom classifier model.
        """
        self.__load_tokenizer()
        self.__load_model(model_name)

    def __load_tokenizer(self) -> None:
        """
        Load Small-E-Czech tokenizer.
        """
        tokenizer_name = "Seznam/small-e-czech"
        logging.debug("Loading tokenizer " + tokenizer_name)
        self.__tokenizer = trf.ElectraTokenizer.from_pretrained(tokenizer_name)
        logging.debug("Tokenizer " + tokenizer_name + " loaded as " + str(self.__tokenizer))

    def __load_model(self, model_name: str) -> None:
        """
        Load custom classifier model based on Small-E-Czech pretrained model.

        :param model_name: name of the custom model in `models` folder.
        """
        model_dir = str("models/" + model_name)
        logging.debug("Loading classifier model " + model_name)
        self.__model = trf.AutoModelForSequenceClassification.from_pretrained(model_dir)
        logging.debug("Model " + model_name + " successfully loaded as " + str(self.__model))
        if torch.cuda.is_available():
            self.__model.to('cuda')
            logging.info("Model is using GPU")

    def get_predictions(self, fulltexts: list[str]) -> Tensor:
        """
        Classify sentiment of given list of fulltexts.

        :param fulltexts: list of document fulltexts.
        :return: sentiment predictions.
        """
        inputs = self.__tokenizer(fulltexts, return_tensors="pt",
                                  padding='longest', truncation=True, max_length=512)
        if torch.cuda.is_available():
            inputs.to('cuda')
        outputs = self.__model(**inputs, output_hidden_states=False, output_attentions=False)
        predictions = torch.nn.functional.softmax(outputs.logits, dim=1)
        logging.debug("Predictions returned from the model: " + str(predictions.tolist()))
        return predictions

    def __call__(self, docs: list[dict], function: Callable[[Tensor], Any]) -> list[dict]:
        """
        Classify sentiment of given list of documents.

        :param docs: list of documents with `contentid` and `fulltext` keys.
        :param function: converter from prediction scores to sentiment value.
        :return: list of documents with `contentid` key and sentiment value in `sentiment` key.
        """
        contentids, fulltexts = lists_from_docs(docs)
        predictions = self.get_predictions(fulltexts)
        sentiment_values = map(function, predictions)
        return docs_from_lists(contentids, list(sentiment_values))
