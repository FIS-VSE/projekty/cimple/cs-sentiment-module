#!/usr/bin/env python
# encoding: utf-8
"""
Command-line interface for a CS Sentiment package

:author: Filip Vencovsky
:copyright: (c) 2022 Prague University of Economics and Business.
:license: MIT, see LICENSE for more details.
"""

import getopt
import json
import logging
import sys

from .cs_sentiment import cs_sentiment


def parse_arguments(argv: list[str]) -> dict:
    """
    Parse command line arguments and create dictionary.

    :param argv: List of string arguments, usually passed by `sys.argv`.
    :return: Dictionary of parsed arguments
    """
    help_message = "python -m cs_sentiment -c <configfile>"
    configfile = ""
    try:
        if len(argv) < 2:
            raise getopt.GetoptError("At least one argument required. "
                                     "Usage: " + help_message)
        argv = argv[1:]
        opts, args = getopt.getopt(argv, "hc:", ["help", "config="])
    except getopt.GetoptError as err:
        logging.error(err.msg)
        sys.exit(1)
    for opt, arg in opts:
        if opt == ("-h", "--help"):
            print(help_message)
            sys.exit()
        elif opt in ("-c", "--config"):
            configfile = arg
    logging.info("Using config file " + configfile)
    return {"configfile": configfile}


def parse_config(config_path: str) -> dict:
    """
    Parse config file in json format and return configuration.

    Required keys are checked during parsing:
        ['model', 'sentiment_value', 'username', 'password', 'logfile',
        'base_url', 'endpoint_search', 'endpoint_update',
        'search_common', 'search_config_stubs', "sleep_after_update"]

    :param config_path: Path to a config file.
    :return: Configuration dictionary.
    """
    required_keys = ['model', 'sentiment_value', 'username', 'password', 'logfile',
                     'base_url', 'endpoint_search', 'endpoint_update',
                     'search_common', 'search_config_stubs', "sleep_after_update"]
    try:
        with open(config_path) as config_file:
            parsed_config = json.load(config_file)
            for key in required_keys:
                if key not in parsed_config:
                    raise KeyError(key)
            logging.debug("Config successfully loaded: " + json.dumps(parsed_config))
            return parsed_config
    except FileNotFoundError as err:
        logging.error("Config field '" + err.filename + "'. " + err.strerror)
        sys.exit(1)
    except json.decoder.JSONDecodeError as err:
        logging.error("Config file is not valid. " + err.msg)
        sys.exit(1)
    except KeyError as err:
        logging.error("Missing key '" + err.args[0] + "' in config file.")
        sys.exit(1)


def main() -> None:
    """
    Parse arguments from command line and call main package function `cs_sentiment`.
    """
    logging.basicConfig(level=logging.DEBUG)

    # parse arguments and create configuration
    args = parse_arguments(sys.argv)
    config = parse_config(args["configfile"])

    # call the main function of the package
    cs_sentiment(config)


if __name__ == '__main__':
    main()
